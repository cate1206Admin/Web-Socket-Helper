﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 内容类型
    /// </summary>
    public enum MessageContentType
    {
        /// <summary>
        /// 发送文本
        /// </summary>
        SendText = 0,

        /// <summary>
        /// 发送表情
        /// </summary>
        SendIcon = 8,

        /// <summary>
        /// 发送图片
        /// </summary>
        SendImage = 1,

        /// <summary>
        /// 发送语音
        /// </summary>
        SendVoice = 2,

        /// <summary>
        /// 发送视频
        /// </summary>
        SendVideo = 3,

        /// <summary>
        /// 发送文件
        /// </summary>
        SendFile = 4,

        /// <summary>
        /// 发送超链接
        /// </summary>
        SendHref = 5,

        /// <summary>
        /// 发送一个HTML
        /// </summary>
        SendHtml = 6,

        /// <summary>
        /// 发送地图
        /// </summary>
        SendMap = 7,

        /// <summary>
        /// 自定义消息
        /// </summary>
        SendPro = 9

    }

}
