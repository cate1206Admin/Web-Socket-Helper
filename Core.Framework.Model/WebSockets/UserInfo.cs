﻿namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 用户信息
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// 用户标识
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 订阅消息KEY
        /// </summary>
        public string[] Subscription { get; set; }

        /// <summary>
        /// 自定义参数
        /// </summary>
        public string Parameter { get; set; }
    }

}
