﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.ViewModel
{
    /// <summary>
    /// 用户登陆
    /// </summary>
    public class ScoketUserRequest
    {
        /// <summary>
        /// User ID
        /// </summary>
        public int key { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public int hour { get; set; } = 24 * 3;

        /// <summary>
        /// 用户名
        /// </summary>
        public string user { get; set; }

        /// <summary>
        /// UUID
        /// </summary>
        public string uuid { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 主体
        /// </summary>
        public string Template { get; set; } = "WebSocket";

        /// <summary>
        /// 微信Token
        /// </summary>
        public string wchat { get; set; }

        /// <summary>
        /// 用户Token
        /// </summary>
        public string token { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }

        /// <summary>
        /// 项目Token
        /// </summary>
        public string projectToken { get; set; }

        /// <summary>
        /// 扩展参数
        /// </summary>
        public string parameter { get; set; }

        /// <summary>
        /// WS 回调地址
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 是否登陆 webscoket 身份
        /// </summary>
        public bool IsLoginWs { get; set; }

    }
}
