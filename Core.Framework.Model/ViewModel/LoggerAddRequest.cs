namespace Core.Framework.Model.ViewModel
{
    public class LoggerAddRequest
    {
        /// <summary>
        /// Tag标签
        /// </summary>
        public string tag { get; set; }

        /// <summary>
        /// 日志主题
        /// </summary>
        public string template { get; set; }

        /// <summary>
        /// 日志内容
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 日志旗帜
        /// </summary>
        public string logFlag { get; set; }
    }
}
