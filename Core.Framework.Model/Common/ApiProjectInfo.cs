﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.Common
{
    /// <summary>
    /// 项目信息
    /// </summary>
    public class ApiProjectInfo
    {
        /// <summary>
        /// 应用关键字
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// 应用Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 应用名称
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 支付宝支付参数
        /// </summary>
        public PayAliInfo PayAliInfo { get; set; }

        /// <summary>
        /// 微信支付参数
        /// </summary>
        public PayWChatInfo PayWChatInfo { get; set; }

    }
}
