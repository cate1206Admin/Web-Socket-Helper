﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace Core.Framework.Model.Common
{
    /// <summary>
    /// Core startup helper.
    /// </summary>
    public class CoreStartupHelper
    {

        private static bool? _IsDebug { get; set; } = null;

        /// <summary>
        /// 是否是Debug环境
        /// </summary>
        public static bool IsDebug {
            get
            {
                if(_IsDebug == null)
                    _IsDebug = GetConfigValue("Service:Debug") == "True";

                return _IsDebug == true;
            }
        }


        /// <summary>
        /// 配置信息
        /// </summary>
        /// <value>The configuration.</value>
        public static IConfiguration Configuration { get; set; }


        /// <summary>
        /// 获配置链接信息
        /// </summary>
        /// <returns>The config value.</returns>
        /// <param name="nodeName">Node name.</param>
        public static string GetConnectionValue(string nodeName)
        {
            return Configuration.GetConnectionString(nodeName);
        }

        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        public static string GetConfigValue(string nodeName)
        {
            return Configuration.GetValue<string>(nodeName);
        }
    }
}
