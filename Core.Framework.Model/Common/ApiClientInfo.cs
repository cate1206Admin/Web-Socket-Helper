﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.Common
{
    /// <summary>
    /// 连接客户机信息
    /// </summary>
    public class ApiClientInfo
    {
        /// <summary>
        /// 用户关键字
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// 用户令牌
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 客户端IP地址
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 项目令牌
        /// </summary>
        public string ProjectToken { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime? EndTime { get; set; }

    }
}
