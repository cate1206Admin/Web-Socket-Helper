﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Core.Framework.Util
{
    public static partial class Attributes
    {
        /// <summary>
        /// 数据验证
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Tuple<bool, string> IsValid<T>(this T model, IsValidEnum e = IsValidEnum.init) where T : class
        {

            if (null == model)
            {
                return new Tuple<bool, string>(false, "实体为NULL");
            }

            Type type = model.GetType();

            // 验证字段
            foreach (var field in type.GetFields())
            {
                if (field.IsDefined(typeof(ValidationAttribute), true))
                {
                    var attrs = Attribute.GetCustomAttributes(field, typeof(ValidationAttribute), true);
                    foreach (ValidationAttribute attr in attrs)
                        if (attr is CustomizeValidationAttribute)
                        {
                            var attrc = (CustomizeValidationAttribute)attr;
                            if(attrc.IsValidType.Equals(e))
                                if (!attr.IsValid(field.GetValue(model)))
                                    return new Tuple<bool, string>(false, attr.ErrorMessage);
                        }
                        else
                            if (!attr.IsValid(field.GetValue(model)))
                                return new Tuple<bool, string>(false, attr.ErrorMessage);
                }
            }

            // 验证属性
            foreach (var property in type.GetProperties())
            {
                if (property.IsDefined(typeof(ValidationAttribute), true))
                {
                    var attrs = Attribute.GetCustomAttributes(property, typeof(ValidationAttribute), true);
                    foreach (ValidationAttribute attr in attrs)
                        if (attr is CustomizeValidationAttribute)
                        {
                            var attrc = (CustomizeValidationAttribute)attr;
                            if (attrc.IsValidType.Equals(e))
                                if (!attr.IsValid(property.GetValue(model)))
                                    return new Tuple<bool, string>(false, attr.ErrorMessage);
                        }
                        else
                        if (!attr.IsValid(property.GetValue(model)))
                            return new Tuple<bool, string>(false, attr.ErrorMessage);
                }

                switch (e)
                {
                    case IsValidEnum.update:

                        string[] updatelist = new string[] { "UpdateLastTime" };

                        if (updatelist.Contains(property.Name))
                            property.SetValue(model, DateTime.Now);

                        break;

                    case IsValidEnum.reg:

                        string[] reglist = new string[] { "AddTime", "Addtime", "RegTime" };

                        if (reglist.Contains(property.Name))
                            property.SetValue(model, DateTime.Now);

                        break;
                }

            }

            return new Tuple<bool, string>(true, "");
        }

        /// <summary>
        /// 数据验证
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Tuple<bool, string> IsValid<T>(this List<T> list, IsValidEnum e = IsValidEnum.init) where T : class
        {
            foreach (var item in list)
            {
                var result = IsValid(item,e);
                if (!result.Item1)
                {
                    return result;
                }
            }
            
            return new Tuple<bool, string>(true, "");
        }



    }

    /// <summary>
    /// 验证类型
    /// </summary>
    public enum IsValidEnum
    {
        /// <summary>
        /// 默认
        /// </summary>
        init,
        /// <summary>
        /// 注册
        /// </summary>
        reg,
        /// <summary>
        /// 修改
        /// </summary>
        update,
        /// <summary>
        /// 删除
        /// </summary>
        delete,
        /// <summary>
        /// 查询时
        /// </summary>
        select
    }
}
