﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.ApiClient.Controllers.Socket.AcceptModel
{
    /// <summary>
    /// 短信参数
    /// </summary>
    public class SMSRequest
    {
        /// <summary>
        /// 接收方手机 
        /// </summary>
        public string toPhone { get; set; }

    }
}
