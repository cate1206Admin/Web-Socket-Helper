﻿using Core.Framework.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.ApiClient.Controllers.Socket.AcceptModel
{
    /// <summary>
    /// 用户查询参数
    /// </summary>
    public class UserSearchRequest
    {
        /// <summary>
        /// 查询关键字
        /// </summary>
        public string search { get; set; }

        /// <summary>
        /// 起始时间
        /// </summary>
        public string startTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public string endTime { get; set; }


        /// <summary>
        /// 每页显示条数
        /// </summary>
        public int rows { get; set; } = 20;

        /// <summary>
        /// 页名
        /// </summary>
        public int page { get; set; } = 1;


        /// <summary>
        /// 分页对象
        /// </summary>
        public Pagination pagination { get; set; }


        /// <summary>
        /// 兼容 Layui Table
        /// </summary>
        public void LayerTable()
        {
            this.pagination.rows = this.pagination.rows == 0 ? this.rows : this.pagination.rows;
            this.pagination.page = this.pagination.page == 0 ? this.page : this.pagination.page;
        }
    }
}
