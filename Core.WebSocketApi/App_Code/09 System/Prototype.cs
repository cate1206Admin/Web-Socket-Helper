﻿using Core.ApiClient.BaseAttribute;
using System.Collections;
using System.Reflection;

namespace System
{
    public static class Prototype
    {
        /// <summary>
        /// 获取备注信息
        /// </summary>
        /// <returns></returns>
        public static string GetRemark<T>(this T tIn)
        {
            Type type = typeof(Enum);

            if (tIn.GetType().BaseType.Equals(type))
            {
                FieldInfo field = tIn.GetType().GetField(tIn.ToString());
                RemarkAttribute attribute =
                    Attribute.GetCustomAttribute(field, typeof(RemarkAttribute)) as RemarkAttribute;
                return attribute.Info;
            }

            object[] objAttrs = tIn.GetType().GetCustomAttributes(typeof(RemarkAttribute), true);
            if (objAttrs.Length > 0)
                return (objAttrs[0] as RemarkAttribute).Info;
            return string.Empty;
        }
    }
}
