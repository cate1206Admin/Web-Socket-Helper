﻿using Core.ApiClient;
using Core.Framework.Model.Common;
using Core.Framework.Model.WebSockets;
using Core.Framework.Redis;
using Core.Framework.Redis.Queue_Helper;
using Core.Framework.Util;
using Core.Framework.Util.Common;
using Core.IBusiness.ILoggerModule;
using Core.IBusiness.ISocketModule;
using Core.Middleware.WebSockets.TimerJobs;
using Core.Service.TaskHandle;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Quartz.Impl.Triggers;

namespace Core.ApiClient
{
    [Obsolete("暂不使用")]
    public class AuthorizeRoutesMidWare
    {
        private readonly RequestDelegate requestDelegate;


        public AuthorizeRoutesMidWare(RequestDelegate requestDelegate)
        {
            this.requestDelegate = requestDelegate;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path == "/authorize")
            {

                var token = context.Request.Headers["itool-token"].ToString();

                if (!string.IsNullOrWhiteSpace(token))
                {
                    var result
                        = RedisQueueHelper.HashGet(RedisQueueHelperParameter.ApiClientByToken, token);

                    if (string.IsNullOrWhiteSpace(result))
                    {
                        await context.Response.WriteAsync(new JsonResult(new
                        {
                            code = CodeResult.PERMISSION_NO_ACCESS,
                            info = "没有访问权限"
                        }).TryToJson());
                    }
                    else {
                        await context.Response.WriteAsync(new JsonResult(new
                        {
                            code = CodeResult.SUCCESS
                        }).TryToJson());
                    }
                }
                else
                {
                    await context.Response.WriteAsync(new JsonResult(new
                    {
                        code = CodeResult.SUCCESS
                    }).TryToJson());
                }

                
            }
            else
            {
                await requestDelegate(context);
            }
        }

    }


}
