﻿using Core.ApiClient.Model;
using Core.DataAccess.Model.Project.Queue;
using Core.DataAccess.Model.Projects;
using Core.Framework.Model.Common;
using Core.Framework.Redis;
using Core.IBusiness.ILoggerModule;
using Core.IBusiness.IProjectModule;
using Core.IBusiness.ISocketModule;
using Core.IBusiness.IUserModule;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Core.ApiClient
{
    /// <summary>
    /// Token 验证
    /// </summary>
    [ApiAuthorize]
    public class BaseApiController : ControllerBase
    {

        public BaseApiController()
        {
            
        }


        private IProject _iProject;
        private IProjectUser _iProjectUser;
        private ISocketGroup _iSocketGroup;
        private ISocketRelationship _iSocketRelationship;
        private ISocketMessage _iSocketMessage;
        private ISocketUser _iSocketUser;
        private ILog _iLog;

        private IQueue _iQueue;

        private IRedisHelper _iRedis;
        private ProjectContext _iProjectContext;
        private ProjectSocketContext _iProjectSocketContext;



        /// <summary>
        /// Redis 缓存
        /// </summary>
        protected IRedisHelper iRedis
        {
            get
            {
                if (this._iRedis == null)
                    this._iRedis = BuildServiceProvider.GetService<IRedisHelper>();
                return _iRedis;
            }
        }


        /// <summary>
        /// 消息队列
        /// </summary>
        protected IQueue iQueue
        {
            get
            {
                if (this._iQueue == null)
                    this._iQueue = BuildServiceProvider.GetService<IQueue>();
                return _iQueue;
            }
        }

        /// <summary>
        /// 项目日志
        /// </summary>
        protected ILog iLog
        {
            get
            {
                if (this._iLog == null)
                    this._iLog = BuildServiceProvider.GetService<ILog>();
                return _iLog;
            }
        }

        /// <summary>
        /// 项目信息|操作
        /// </summary>
        protected IProject iProject
        {
            get
            {
                if (this._iProject == null)
                    this._iProject = BuildServiceProvider.GetService<IProject>();
                return _iProject;
            }
        }

        /// <summary>
        /// 用户操作
        /// </summary>
        protected IProjectUser iProjectUser
        {
            get
            {
                if (this._iProjectUser == null)
                    this._iProjectUser = BuildServiceProvider.GetService<IProjectUser>();
                return _iProjectUser;
            }
        }

        /// <summary>
        /// 群组管理
        /// </summary>
        protected ISocketGroup iSocketGroup
        {
            get
            {
                if (this._iSocketGroup == null)
                    this._iSocketGroup = BuildServiceProvider.GetService<ISocketGroup>();
                return _iSocketGroup;
            }
        }

        /// <summary>
        /// 用户关系管理
        /// </summary>
        protected ISocketRelationship iSocketRelationship
        {
            get
            {
                if (this._iSocketRelationship == null)
                    this._iSocketRelationship = BuildServiceProvider.GetService<ISocketRelationship>();
                return _iSocketRelationship;
            }
        }

        /// <summary>
        /// Queue消息管理
        /// </summary>
        protected ISocketMessage iSocketMessage
        {
            get
            {
                if (this._iSocketMessage == null)
                    this._iSocketMessage = BuildServiceProvider.GetService<ISocketMessage>();
                return _iSocketMessage;
            }

        }

        /// <summary>
        /// Queue 用户操作
        /// </summary>
        protected ISocketUser iSocketUser
        {
            get
            {
                if (this._iSocketUser == null)
                    this._iSocketUser = BuildServiceProvider.GetService<ISocketUser>();
                return _iSocketUser;
            }
        }







        /// <summary>
        /// 项目管理数据库
        /// </summary>
        protected ProjectContext iProjectContext
        {
            get
            {
                if (this._iProjectContext == null)
                    this._iProjectContext = BuildServiceProvider.GetService<ProjectContext>();
                return _iProjectContext;
            }
        }

        /// <summary>
        /// 消息管理数据库
        /// </summary>
        protected ProjectSocketContext iProjectSocketContext
        {
            get
            {
                if (this._iProjectSocketContext == null)
                    this._iProjectSocketContext = BuildServiceProvider.GetService<ProjectSocketContext>();
                return _iProjectSocketContext;
            }
        }



        /// <summary>
        /// 客户端信息
        /// </summary>
        protected ApiClientInfo iClientInfo
        {
            get
            {
                var info = (ApiClientInfo)HttpContext.Items["ClientInfo"] ?? new ApiClientInfo();
                info.Ip = (string)HttpContext.Items["RemoteIpAddress"];
                return info;
            }
        }

        /// <summary>
        /// 项目信息
        /// </summary>
        protected ApiProjectInfo iProjectInfo
        {
            get
            {
                return new ApiProjectInfo
                {
                    Key = 1,
                    Title = "系统项目",
                    Token = "system"
                };

                return (ApiProjectInfo)HttpContext.Items["ApiProjectInfo"] ?? new ApiProjectInfo();
            }
        }


        #region Common

        /// <summary>
        /// 通用 Result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <param name="pagination">分页参数</param>
        /// <returns></returns>
        protected IResult Result<T>(MethodResult<T> result, Pagination pagination = null)
        {

            if (pagination == null)
            {
                return new ApiResult
                {
                    code = result.IsFinished ?
                        CodeResult.SUCCESS :
                        CodeResult.BUSINESS_ERROR,
                    info = result.IsFinished ? null : result.Discription,
                    date = result.Date
                };
            }
            else
            {
                return new ApiResult
                {
                    code = result.IsFinished ?
                        CodeResult.SUCCESS :
                        CodeResult.BUSINESS_ERROR,
                    info = result.IsFinished ? null : result.Discription,
                    date = new
                    {
                        pagination = pagination,
                        data = result.Date
                    }
                };
            }
            
        }

        /// <summary>
        /// 通用执行方法
        /// </summary>
        /// <param name="result"></param>
        /// <param name="isValids">验证模型</param>
        /// <returns></returns>
        protected IResult ExecMethod(
            Func<IResult> result, params Tuple<bool, string>[]  isValids)
        {

            // 数据验证
            if (null != isValids && isValids.Length > 0)
                foreach (var isValid in isValids)
                {
                    if (!isValid.Item1)
                    {
                        return new ApiResult
                        {
                            code = CodeResult.DATA_IS_WRONG,
                            date = isValid.Item2
                        };
                    }
                }

            

            return result.Invoke();
        }

        #endregion

    }
}
