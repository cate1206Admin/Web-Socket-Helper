﻿using Core.DataAccess.Model.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule
{
    
    /// <summary>
    /// 项目用户模块
    /// </summary>
    public interface IProjectUser
    {
        /// <summary>
        /// 账户密码登陆
        /// </summary>
        /// <param name="user">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="hour">项目TOKEN时效性</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        Tuple<ProjectUser, bool> Login(string user, string password,int hour);

        /// <summary>
        /// 根据用户TOKNE登陆
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="password">密码</param>
        /// <param name="hour">token时效性</param>
        /// <returns></returns>
        Tuple<ProjectUser, bool> LoginByToken(string token, string password,int hour);

        /// <summary>
        /// 修改用户信息
        /// 查找 [用户名和密码] [OPENID]
        /// 相匹配的用户
        /// </summary>
        /// <param name="model">用户信息</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        ProjectUser UpdateUserInfo(ProjectUser model);

        /// <summary>
        /// 用户注册
        /// 校验 [手机是否重复]
        /// </summary>
        /// <param name="model">用户信息</param>
        Tuple<bool, ProjectUser, string> Reg(ProjectUser model);

        /// <summary>
        /// 退出登陆
        /// </summary>
        /// <param name="token">登陆token</param>
        /// <returns></returns>
        void OutLogin(string token);

        /// <summary>
        /// 根据临时TOKNE获取用户信息
        /// </summary>
        /// <param name="token">临时Token</param>
        /// <returns>是否过期|用户信息</returns>
        Tuple<bool,ProjectUser> GetUserInfoByToken(string token);

    }
}
