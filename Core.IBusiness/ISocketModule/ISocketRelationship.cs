﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DataAccess.Model.Project.Queue;

namespace Core.IBusiness.ISocketModule
{
    /// <summary>
    /// 好友关系管理
    /// </summary>
    public interface ISocketRelationship
    {
        /// <summary>
        /// 查询用户关系
        /// [分组列表]
        /// [好友列表]
        /// </summary>
        /// <param name="userKey"></param>
        /// <param name="projectToken">项目token</param>
        /// <returns></returns>
        Tuple<List<ProjectUserGroup>,List<ProjectUserGroupMember>, bool> GetAll(string userKey,string projectToken);

        /// <summary>
        /// 查询指定得分组好友
        /// </summary>
        /// <param name="groupKey">分组 ID</param>
        /// <param name="projectToken">项目token</param>
        /// <returns></returns>
        Tuple<List<ProjectUserGroupMember>, bool> GetGroupMembersByGroupKey(int groupKey, string projectToken);

        /// <summary>
        /// 查询指定好友信息
        /// </summary>
        /// <param name="id">好友关键字</param>
        /// <param name="projectToken">项目token</param>
        /// <returns></returns>
        Tuple<ProjectUserGroupMember, bool> GetItemGroupMember(int id, string projectToken);


        /// <summary>
        /// 创建分组
        /// </summary>
        /// <param name="model">分组模型</param>
        /// <returns></returns>
        Tuple<ProjectUserGroup, string, bool> CreateGroup(ProjectUserGroup model);


        /// <summary>
        /// 修改分组
        /// </summary>
        /// <param name="model">分组模型</param>
        /// <returns></returns>
        Tuple<ProjectUserGroup, string, bool> UpdateGroup(ProjectUserGroup model);


        /// <summary>
        /// 删除分组
        /// </summary>
        /// <param name="id">分组关键字</param>
        /// <param name="projectToken">项目token</param>
        /// <returns></returns>
        Tuple<ProjectUserGroup, bool> DeleteGroup(int id, string projectToken);

        /// <summary>
        /// 创建成员
        /// </summary>
        /// <param name="model">分组成员模型</param>
        /// <returns></returns>
        Tuple<ProjectUserGroupMember, string, bool> CreateGroupMember(ProjectUserGroupMember model);


        /// <summary>
        /// 修改成员
        /// </summary>
        /// <param name="model">分组成员模型</param>
        /// <returns></returns>
        Tuple<ProjectUserGroupMember, string, bool> UpdateGroupMember(ProjectUserGroupMember model);


        /// <summary>
        /// 删除成员
        /// </summary>
        /// <param name="id">分组成员关键字</param>
        /// <param name="projectToken">项目token</param>
        /// <returns></returns>
        Tuple<ProjectUserGroupMember, bool> DeleteGroupMember(int id, string projectToken);
    }
}
