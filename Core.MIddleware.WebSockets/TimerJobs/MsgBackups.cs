﻿using Quartz;
using Core.Framework.Model.Common;
using Core.Framework.Redis.Queue_Helper;
using Core.Framework.Util.Common;
using Core.IBusiness.ISocketModule;

namespace Core.Middleware.WebSockets.TimerJobs
{
    /// <summary>
    /// 消息备份
    /// </summary>
    public class MsgBackups : ITimerJob
    {
        public void Execute(IJobExecutionContext context)
        {
            ISocketMessage iQueueMessage =
                BuildServiceProvider.GetService<ISocketMessage>();

            iQueueMessage
                .SaveSingleOffLine(
                    RedisQueueHelper
                        .GetListPopByLength(
                            RedisQueueHelperParameter.SingleNull, 0, 99)
                    );

            iQueueMessage
                .SaveHistory(
                    RedisQueueHelper
                        .GetListPopByLength(
                            RedisQueueHelperParameter.History, 0, 500 * 2, force: true)
                    );
        }
    }
}
