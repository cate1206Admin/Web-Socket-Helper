﻿using Core.DataAccess.Model.Project.Logs;
using Core.Framework.Model.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace Core.DataAccess.Model.ProjectLogs
{
    /// <summary>
    /// 日志模块
    /// </summary>
    public partial class ProjectLogsContext : DbContext
    {
        public ProjectLogsContext()
        {
        }

        public ProjectLogsContext(DbContextOptions<ProjectLogsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Logger> Logger { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLoggerFactory(EFLogger.GetLoggerFactory())
                    .UseSqlServer(CoreStartupHelper.GetConnectionValue("Logs_SqlServerConnection"), a => a.UseRowNumberForPaging());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }


    }
}
