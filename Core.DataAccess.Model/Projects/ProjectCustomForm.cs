﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectCustomForm
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? ProjectKey { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
