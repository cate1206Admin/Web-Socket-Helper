﻿using Core.DataAccess.Model.ProjectLogs;
using Core.Framework.Model.Common;
using Microsoft.EntityFrameworkCore;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectContext : DbContext
    {
        public ProjectContext()
        {
        }

        public ProjectContext(DbContextOptions<ProjectLogsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ConfigDic> ConfigDic { get; set; }

        public virtual DbSet<ConfigProject> ConfigProject { get; set; }

        public virtual DbSet<ProjectApi> ProjectApi { get; set; }
        public virtual DbSet<ProjectClient> ProjectClient { get; set; }
        public virtual DbSet<ProjectCustomForm> ProjectCustomForm { get; set; }
        public virtual DbSet<ProjectCustomFormList> ProjectCustomFormList { get; set; }
        public virtual DbSet<ProjectCustomFormListDate> ProjectCustomFormListDate { get; set; }
        public virtual DbSet<ProjectDb> ProjectDb { get; set; }
        public virtual DbSet<ProjectDbTabel> ProjectDbTabel { get; set; }
        public virtual DbSet<ProjectDbTabelCopy> ProjectDbTabelCopy { get; set; }

        /// <summary>
        /// 项目列表
        /// </summary>
        public virtual DbSet<ProjectList> ProjectList { get; set; }
        public virtual DbSet<ProjectRunningLog> ProjectRunningLog { get; set; }

        /// <summary>
        /// 注册用户
        /// </summary>
        public virtual DbSet<ProjectUser> ProjectUser { get; set; }

        /// <summary>
        /// 用户购买的APi
        /// </summary>
        public virtual DbSet<ProjectUserApi> ProjectUserApi { get; set; }

        /// <summary>
        /// 购买的API日志
        /// </summary>
        public virtual DbSet<ProjectUserApiLog> ProjectUserApiLog { get; set; }

        /// <summary>
        /// API订单
        /// </summary>
        public virtual DbSet<ProjectUserApiOrder> ProjectUserApiOrder { get; set; }


        public virtual DbSet<ProjectWps> ProjectWps { get; set; }
        public virtual DbSet<ProjectWpsList> ProjectWpsList { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var Project_SqlServerConnection = CoreStartupHelper.GetConnectionValue("Project_SqlServerConnection");

                optionsBuilder
                    // 数据操作日志
                    .UseLoggerFactory(EFLogger.GetLoggerFactory())
                    .UseSqlServer(Project_SqlServerConnection, a => a.UseRowNumberForPaging());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<ConfigDic>(entity =>
            {
                entity.ToTable("Config_Dic");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.UpKey).HasColumnName("upKey");
            });

            modelBuilder.Entity<ConfigProject>(entity =>
            {
                entity.ToTable("Config_Project");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Config)
                    .HasColumnName("config")
                    .IsUnicode(false);

                entity.Property(e => e.ProjectId).HasColumnName("projectId");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnName("token")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<ProjectApi>(entity =>
            {
                entity.ToTable("Project_Api");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Info)
                    .IsRequired()
                    .HasColumnName("info")
                    .HasMaxLength(350)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectClient>(entity =>
            {
                entity.ToTable("Project_Client");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Android)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Db)
                    .HasColumnName("DB")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Ios)
                    .HasColumnName("IOS")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Pc)
                    .HasColumnName("PC")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectKey).HasColumnName("projectKey");

                entity.Property(e => e.Wap)
                    .HasColumnName("WAP")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Weixin)
                    .HasColumnName("WEIXIN")
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectCustomForm>(entity =>
            {
                entity.ToTable("Project_Custom_Form");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProjectKey).HasColumnName("projectKey");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectCustomFormList>(entity =>
            {
                entity.ToTable("Project_Custom_Form_List");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomFormKey).HasColumnName("customFormKey");

                entity.Property(e => e.DateType).HasColumnName("dateType");

                entity.Property(e => e.DateTypeKey).HasColumnName("dateTypeKey");

                entity.Property(e => e.Def)
                    .HasColumnName("def")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsUsing).HasColumnName("isUsing");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectCustomFormListDate>(entity =>
            {
                entity.ToTable("Project_Custom_Form_List_Date");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.CustomFormListKey).HasColumnName("customFormListKey");

                entity.Property(e => e.ItemDate)
                    .HasColumnName("itemDate")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectDb>(entity =>
            {
                entity.ToTable("Project_DB");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DbName)
                    .HasColumnName("dbName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PassWord)
                    .HasColumnName("passWord")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectKey).HasColumnName("projectKey");

                entity.Property(e => e.Self).HasColumnName("self");

                entity.Property(e => e.Sever)
                    .HasColumnName("sever")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasColumnName("userName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectDbTabel>(entity =>
            {
                entity.ToTable("Project_DB_Tabel");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ColInfoList)
                    .HasColumnName("colInfoList")
                    .HasMaxLength(550)
                    .IsUnicode(false);

                entity.Property(e => e.ColName)
                    .HasColumnName("colName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ColType)
                    .HasColumnName("colType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DbKey).HasColumnName("dbKey");

                entity.Property(e => e.Dec).HasColumnName("dec");

                entity.Property(e => e.Def)
                    .HasColumnName("def")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Href)
                    .HasColumnName("href")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Info)
                    .HasColumnName("info")
                    .IsUnicode(false);

                entity.Property(e => e.IsNull).HasColumnName("isNull");

                entity.Property(e => e.Lenght)
                    .HasColumnName("lenght")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryKry).HasColumnName("primaryKry");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpKey).HasColumnName("upKey");
            });

            modelBuilder.Entity<ProjectDbTabelCopy>(entity =>
            {
                entity.ToTable("Project_DB_Tabel_Copy");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ColInfoList)
                    .HasColumnName("colInfoList")
                    .HasMaxLength(550)
                    .IsUnicode(false);

                entity.Property(e => e.ColName)
                    .HasColumnName("colName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ColType)
                    .HasColumnName("colType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DbKey).HasColumnName("dbKey");

                entity.Property(e => e.Dec).HasColumnName("dec");

                entity.Property(e => e.Def)
                    .HasColumnName("def")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Href)
                    .HasColumnName("href")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Info)
                    .HasColumnName("info")
                    .IsUnicode(false);

                entity.Property(e => e.IsNull).HasColumnName("isNull");

                entity.Property(e => e.Lenght)
                    .HasColumnName("lenght")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryKry).HasColumnName("primaryKry");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpKey).HasColumnName("upKey");
            });

            modelBuilder.Entity<ProjectList>(entity =>
            {
                entity.ToTable("Project_List");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Info)
                    .HasColumnName("info")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Ios).HasColumnName("IOS");

                entity.Property(e => e.Pc).HasColumnName("PC");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(21)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(right(newid(),(21)))");

                entity.Property(e => e.UserKey).HasColumnName("userKey");

                entity.Property(e => e.Wap).HasColumnName("WAP");
            });

            modelBuilder.Entity<ProjectRunningLog>(entity =>
            {
                entity.ToTable("Project_Running_Log");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("content")
                    .IsUnicode(false);

                entity.Property(e => e.ProjectToken)
                    .IsRequired()
                    .HasColumnName("projectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.UserPhone)
                    .IsRequired()
                    .HasColumnName("userPhone")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.UserToken)
                    .IsRequired()
                    .HasColumnName("userToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectUser>(entity =>
            {
                entity.ToTable("Project_User");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndTime)
                    .HasColumnName("endTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Pass)
                    .IsRequired()
                    .HasColumnName("pass")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(21)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(right(newid(),(21)))");

                entity.Property(e => e.UserName)
                    .HasColumnName("userName")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProjectUserApi>(entity =>
            {
                entity.ToTable("Project_User_Api");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApiCount).HasColumnName("apiCount");

                entity.Property(e => e.ApiDay).HasColumnName("apiDay");

                entity.Property(e => e.ApiFreezeCount).HasColumnName("apiFreezeCount");

                entity.Property(e => e.ApiKey).HasColumnName("apiKey");

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<ProjectUserApiLog>(entity =>
            {
                entity.ToTable("Project_User_Api_Log");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ApiType).HasColumnName("apiType");

                entity.Property(e => e.Content)
                    .HasColumnName("content")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ExecTypeInfo)
                    .HasColumnName("execTypeInfo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserKey).HasColumnName("userKey");
            });

            modelBuilder.Entity<ProjectUserApiOrder>(entity =>
            {
                entity.ToTable("Project_User_Api_Order");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ApiCount)
                    .HasColumnName("apiCount")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ApiDay)
                    .HasColumnName("apiDay")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ApiKey).HasColumnName("apiKey");

                entity.Property(e => e.ApiMoney)
                    .HasColumnName("apiMoney")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Pay).HasColumnName("pay");

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<ProjectWps>(entity =>
            {
                entity.ToTable("Project_WPS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProjectKey).HasColumnName("projectKey");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpKey).HasColumnName("upKey");
            });

            modelBuilder.Entity<ProjectWpsList>(entity =>
            {
                entity.ToTable("Project_WPS_List");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.RequestHead)
                    .HasColumnName("requestHead")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RequestPath)
                    .HasColumnName("requestPath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequestType)
                    .HasColumnName("requestType")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RetExs)
                    .HasColumnName("retExs")
                    .IsUnicode(false);

                entity.Property(e => e.SendExs)
                    .HasColumnName("sendExs")
                    .IsUnicode(false);

                entity.Property(e => e.State).HasColumnName("state");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.WpsKey).HasColumnName("wpsKey");
            });
        }
    }
}
