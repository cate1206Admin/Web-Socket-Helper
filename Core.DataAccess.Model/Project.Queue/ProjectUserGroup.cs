﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Core.DataAccess.Model.Project.Queue
{
    [Description("好友关系管理 - 组")]
    public class ProjectUserGroup
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string ProjectToken { get; set; }
        public DateTime? AddTime { get; set; } = DateTime.Now;
    }
}
