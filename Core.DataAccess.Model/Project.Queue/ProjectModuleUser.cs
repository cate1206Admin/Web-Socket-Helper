﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Core.DataAccess.Model.Project.Queue
{
    [Description("用户信息表")]
    public partial class ProjectModuleUser
    {
        public int Id { get; set; }
        public string Phone { get; set; } = "";
        public string UserId { get; set; }
        public string WxOpenid { get; set; }
        public string AliPayOpenid { get; set; }
        public string BaiduOpenid { get; set; }
        public string UserName { get; set; }
        public string Pass { get; set; } = "";
        public string Token { get; set; }
        public string ProjectToken { get; set; }
        public string Uuid { get; set; }
        public string Paras { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
